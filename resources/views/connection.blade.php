@extends('app')

@section('body')
    <div id="login">
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="" method="post">
                            <h3 class="text-center text-white">Connection</h3>
                            <div class="form-group">
                                <label for="username" class="text-white">Utilisateur</label><br>
                                <input type="text" name="nomutilisateur" id="nomutilisateur" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-white">Mot de Passe:</label><br>
                                <input type="password" name="mdp" id="mdp" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="Connection" class="btn btn-primary btn-block" value="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document ).ready(function() {
            console.log( "Classe donne a body" );
            $("body").addClass("imagebackground");
        });


    </script>
@endsection
