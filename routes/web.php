<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('acceuil');
});
Route::get('/acceuil', function () {
    return view('acceuil');
});

Route::get('/joueur', function () {
    return view('joueur');
});

Route::get('/tournoi', function () {
    return view('tournoi');
});

Route::get('/connection', function () {
    return view('connection');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
